import React from "react";
import "./accout.scss";
const GaioDien = () => {
  return (
    <div className=" GaioDien">
      <nav className="navbar navbar-expand-lg navbar-dark  ">
        <div className="container px-lg-5">
          <a className="navbar-brand" href="#">
            Start Bootstrap
          </a>
          <button
            className="navbar-toggler"
            type="button"
            data-toggle="collapse"
            data-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent"
            aria-expanded="false"
            aria-label="Toggle navigation"
          >
            <span className="navbar-toggler-icon" />
          </button>
          <div
            className=" collapse navbar-collapse"
            id="navbarSupportedContent"
          >
            <ul className="navbar-nav mr-auto  ">
              <li className="nav-item active">
                <a className="nav-link" href="#">
                  Home
                </a>
              </li>
              <li className="nav-item">
                <a className="nav-link" href="#">
                  About
                </a>
              </li>
              <li className="nav-item">
                <a className="nav-link" href="#">
                  Contact
                </a>
              </li>
            </ul>
          </div>
        </div>
      </nav>
      {/**HEADER */}
      <header className="py-5">
        <div className="container px-lg-5">
          <div className="p-4 p-lg-5 bg-light rounded-3 text-center">
            <div className="m-4 m-lg-5">
              <h1>A warm welcome!</h1>
              <p className="fs-4">
                Bootstrap utility classes are used to create this jumbotron
                since <br /> the old component has been removed from the
                framework. Why <br />
                create custom CSS when you can use utilities?
              </p>
              <button className="btn btn-primary btn-lg">Call to action</button>
            </div>
          </div>
        </div>
      </header>
      <section className="py-4">
        <div className="container px-lg-5">
          {/**List page */}
          <div className="row gx-lg-5">
            <div className="col-lg-6 col-xxl-4 mb-5 rowBg">
              <div className="card bg-light border-0 h-100">
                <div className="card-body text-center p-4 pt-0 pt-lg-0">
                  <div className="feature bg-primary bg-gradient text-white  mb-4 mt-n4">
                    <i class="fa fa-box"></i>
                  </div>
                  <h2> Fresh new layout</h2>
                  <p className="mb-0">
                    With Bootstrap 5, we've created a fresh new <br /> layout
                    for this template!
                  </p>
                </div>
              </div>
            </div>
            <div className="col-lg-6 col-xxl-4 mb-5 rowBg">
              <div className="card bg-light border-0 h-100">
                <div className="card-body text-center p-4 pt-0 pt-lg-0">
                  <div className="feature bg-primary bg-gradient text-white  mb-4 mt-n4">
                    <i class="fa fa-cloud-download-alt"></i>
                  </div>
                  <h2>Free to download</h2>
                  <p className="mb-0">
                    As always, Start Bootstrap has a powerful <br /> collectin
                    of free templates.
                  </p>
                </div>
              </div>
            </div>
            <div className="col-lg-6 col-xxl-4 mb-5 rowBg">
              <div className="card bg-light border-0 h-100">
                <div className="card-body text-center p-4 pt-0 pt-lg-0">
                  <div className="feature bg-primary bg-gradient text-white  mb-4 mt-n4">
                    <i class="fa-solid fa-credit-card"></i>
                  </div>
                  <h2>Jumbotron hero header</h2>
                  <p className="mb-0">
                    The heroic part of this template is the <br /> jumbotron
                    hero header!
                  </p>
                </div>
              </div>
            </div>
            <div className="col-lg-6 col-xxl-4 mb-5 rowBg">
              <div className="card bg-light border-0 h-100">
                <div className="card-body text-center p-4 pt-0 pt-lg-0">
                  <div className="feature bg-primary bg-gradient text-white  mb-4 mt-n4">
                    <i class="fa-brands fa-bootstrap"></i>
                  </div>
                  <h2>Feature boxes</h2>
                  <p className="mb-0">
                    We've created some custom feature boxes <br /> using
                    Bootstrap icons!
                  </p>
                </div>
              </div>
            </div>
            <div className="col-lg-6 col-xxl-4 mb-5 rowBg">
              <div className="card bg-light border-0 h-100">
                <div className="card-body text-center p-4 pt-0 pt-lg-0">
                  <div className="feature bg-primary bg-gradient text-white  mb-4 mt-n4">
                    <i class="fa-solid fa-code"></i>
                  </div>
                  <h2>Simple clean code</h2>
                  <p className="mb-0">
                    We keep our dependencies up to date and <br /> squash bugs
                    as they come!
                  </p>
                </div>
              </div>
            </div>
            <div className="col-lg-6 col-xxl-4 mb-5 rowBg">
              <div className="card bg-light border-0 h-100">
                <div className="card-body text-center p-4 pt-0 pt-lg-0">
                  <div className="feature bg-primary bg-gradient text-white  mb-4 mt-n4">
                    <i class="fa-regular fa-circle-check"></i>
                  </div>
                  <h2>A name you trust</h2>
                  <p className="mb-0">
                    Start Bootstrap has been the leader in free <br /> Bootstrap
                    templates since 2013!
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <footer className="py-5 bg-dark">
        <div className="container">
          <p className="mt-0 text-center text-white">
            Copyright © Your Website 2023
          </p>
        </div>
      </footer>
    </div>
  );
};

export default GaioDien;
