import logo from "./logo.svg";
import "./App.css";
import GaioDien from "./GiaoDien/GaioDien";

function App() {
  return (
    <div>
      <GaioDien />
    </div>
  );
}

export default App;
